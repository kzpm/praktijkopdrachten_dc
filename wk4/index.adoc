:toc:
:toc: left
:toclevels: 3
:toc-title: Inhoud
:icons: font
:terug: https://kbase.nl/datacom/praktijk/

== {terug}[Praktijk DC week 4]

=== Statisch routeren

Onderwerpen:
 
- Configuratie van twee routers en interfaces
- Statisch routeren
- Werken met commandoheets

Nodig:

- Twee switches
- Twee routers
- 4 Ethernet rj45 kabels
- 2 Laptops

NOTE: Voorbereiding: +
- Werk deze opdracht eerst uit in packettracer +
- Maak gebruik van commandsheets. Dus instructies niet rechtstreeks in PT +

[%header, cols="3"]
|===
|Doel
|Zorg voor een geslaagde ping van netwerk 1 naar netwerk 2 en v.v.
|Statische route aanmaken
|Verder ervaring opdoen met commandosheets, documentatie
|===


=== 1.0 Opdracht omschrijving

- Je kunt deze opdracht met twee of drie personen samen uitvoeren
- In dit LAB ga je een router verbinden met een andere router. Op beide routers configureer je de LAN en WAN interfaces (Zie tabel Adressering)
- Vervolgens sluit je op de LAN poorten een switch aan.
- Sluit je laptop met een ethernetkabel aan op een van de switches.
- Geef je ethernetadapter op je laptop een geldig ip adres binnen een van de gegeven subnets (Zie tabel Adressering)


==== 1.1 Adressering

[%header, cols="2,3,3,1"]
|===
|Router|ipadres interface LAN| ip adres interface WAN |Niveau 3/4
|1 |192.168.100.1 255.255.255.0| 10.0.10.1 255.255.255.0 |3
|2 |192.168.200.1 255.255.255.0| 10.0.10.2 255.255.255.0 |3
||||
|1 |192.168.100.1 255.255.255.0| 10.0.10.25 255.255.255.252 |4
|2 |192.168.200.1 255.255.255.0| 10.0.10.26 255.255.255.252 |4
|===


==== 1.2 Klaar?

WARNING: Wanneer je de opdracht succesvol uitgevoerd hebt, reset je de routers met het commando _reload_ en vervolgens typ je _no_. 

==== 1.3 Inleveren

Lever de commandosheets in op de ELO. 
Tegelnaam *T4_21-N4-1 Praktijk*

